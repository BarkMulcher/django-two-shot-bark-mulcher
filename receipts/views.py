from django.shortcuts import render, get_object_or_404, redirect
from receipts.models import Account, Receipt, User, ExpenseCategory
from django.contrib.auth.decorators import login_required
from receipts.forms import (
    AddReceiptForm,
    AddExpenseCatForm,
    AddAccountForm,
    )


# main page (when logged in)
@login_required
def receipt_list(request):
    receipts = Receipt.objects.filter(purchaser=request.user)
    context = {
        'receipt_list': receipts,
    }
    return render(
        request,
        'receipts/index.html',
        context
    )

# create receipt
@login_required
def create_receipt(request):
    if request.method == 'POST':
        form = AddReceiptForm(request.POST)
        if form.is_valid():
            receipt = form.save(False)
            receipt.purchaser = request.user
            receipt.save()
            return redirect('home')
    else:
        form = AddReceiptForm()
    context = {
        'form': form
    }
    return render(request, 'receipts/create_receipt.html', context)

# category list
@login_required
def category_list(request):
    category = ExpenseCategory.objects.filter(owner=request.user)
    context = {
        'category_list': category
    }
    return render(
        request,
        'receipts/categories.html',
        context
    )

# account list
@login_required
def account_list(request):
    account = Account.objects.filter(owner=request.user)
    context = {
        'account_list': account
    }
    return render(
        request,
        'receipts/accounts.html',
        context
    )

@login_required
def create_category(request):
    if request.method == 'POST':
        form = AddExpenseCatForm(request.POST)
        if form.is_valid():
            new_category = form.save(False)
            new_category.owner = request.user
            new_category.save()
            return redirect('category_list')
    else:
        form = AddExpenseCatForm()
    context = {
        'form': form
    }
    return render(
        request,
        'receipts/create_category.html',
        context
    )

@login_required
def create_account(request):
    if request.method == 'POST':
        form = AddAccountForm(request.POST)
        if form.is_valid():
            new_account = form.save(False)
            new_account.owner = request.user
            new_account.save()
            return redirect('account_list')
    else:
        form = AddAccountForm()
    context = {
        'form': form
    }
    return render(
        request,
        'receipts/create_account.html',
        context
    )