from django.forms import ModelForm, Form
from receipts.models import Receipt, ExpenseCategory, Account
from django import forms

class AddReceiptForm(ModelForm):
    class Meta:
        model = Receipt
        fields = [
            'vendor',
            'total',
            'tax',
            'date',
            'category',
            'account'
        ]

class AddExpenseCatForm(ModelForm):
    class Meta:
        model = ExpenseCategory
        fields = [
            'name',
        ]

class AddAccountForm(ModelForm):
    class Meta:
        model = Account
        fields = [
            'name',
            'number',
        ]