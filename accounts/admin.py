# from django.contrib import admin
# from receipts.models import User, Receipt, ExpenseCategory, Account

# # Register your models here.
# @admin.register(User)
# class UserAdmin(admin.ModelAdmin):
#     list_display = [
#         'name',
#     ]

# @admin.register(ExpenseCategory)
# class ExpenseCategoryAdmin(admin.ModelAdmin):
#     list_display = [
#         'name',
#         'owner'
#     ]

# @admin.register(Receipt)
# class ReceiptAdmin(admin.ModelAdmin):
#     list_display = [
#         'vendor',
#         'total',
#         'date',
#         'purchaser',
#         'account'
#     ]

# @admin.register(Account)
# class AccountAdmin(admin.ModelAdmin):
#     list_display = [
#         'name',
#         'number',
#         'owner'
#     ]