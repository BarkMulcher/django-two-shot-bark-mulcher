from django.urls import path
from accounts.views import login_view, logout_view, create_user

urlpatterns = [
    path('login/', login_view, name='login'),
    path('logout/', logout_view, name='logout'),
    path('signup/', create_user, name='signup')
]
